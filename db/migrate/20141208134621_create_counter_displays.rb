class CreateCounterDisplays < ActiveRecord::Migration
  def change
    create_table :counter_displays do |t|
      t.integer :counter_number
      t.string :airline_image
      t.string :background_color

      t.timestamps
    end
  end
end
