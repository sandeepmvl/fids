class CreateRemarks < ActiveRecord::Migration
  def change
    create_table :remarks do |t|
      t.string :flight_status
      t.string :local_name
      t.string :color

      t.timestamps
    end
  end
end
