class CreateAirports < ActiveRecord::Migration
  def change
    create_table :airports do |t|
      t.string :airport_name
      t.string :local_name
      t.string :iata
      t.string :icao

      t.timestamps
    end
  end
end
