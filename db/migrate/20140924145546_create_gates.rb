class CreateGates < ActiveRecord::Migration
  def change
    create_table :gates do |t|
      t.string :gate_number

      t.timestamps
    end
  end
end
