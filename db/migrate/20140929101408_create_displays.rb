class CreateDisplays < ActiveRecord::Migration
  def change
    create_table :displays do |t|
      t.string :display_name
      t.integer :flight_id
      t.string :description
      t.string :gate_number
      t.string :belt_number
      t.string :counter_number
      t.string :viewing
      t.string :flight_numbers
      t.string :schedule_type

      t.timestamps
    end
  end
end
