class CreateFlights < ActiveRecord::Migration
  def change
    create_table :flights do |t|
      t.string :status_type
      t.string :flight_number
      t.string :airline_name
      t.string :airport_name
      t.string :via
      t.integer :airline_id
      t.string :from_to
      t.date :schedule_date
      t.time :schedule_time
      t.date :actual_date
      t.time :actual_time
      t.time :expected_time
      t.string :gate_number
      t.string :counter_number
      t.string :belt_number
      t.string :flight_status
      t.string :bgcolor
      t.timestamps
    end
  end
end
