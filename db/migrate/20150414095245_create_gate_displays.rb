class CreateGateDisplays < ActiveRecord::Migration
  def change
    create_table :gate_displays do |t|
      t.string :airline_image
      t.string :background_color
      t.integer :counter_number

      t.timestamps
    end
  end
end
