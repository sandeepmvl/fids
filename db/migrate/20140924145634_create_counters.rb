class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
      t.string :counter_number

      t.timestamps
    end
  end
end
