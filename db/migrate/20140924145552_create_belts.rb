class CreateBelts < ActiveRecord::Migration
  def change
    create_table :belts do |t|
      t.string :belt_number

      t.timestamps
    end
  end
end
