class CreateAirportDetails < ActiveRecord::Migration
  def change
    create_table :airport_details do |t|
      t.string :airport_name
      t.string :default_message
      t.string :footer_message
      t.string :airport_logo

      t.timestamps
    end
  end
end
