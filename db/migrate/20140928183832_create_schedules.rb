class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :status_type
      t.string :flight_number
      t.string :airline_name
      t.string :airport_name
      t.string :from_to
      t.string :via
      t.time :schedule_time
      t.date :valid_from
      t.date :valid_till
      t.string :available_days
      t.string :gate_number
      t.string :counter_number
      t.string :belt_number

      t.timestamps
    end
  end
end
