# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# user = User.create! :email => 'test@test.com', :password => 'rails123', :password_confirmation => 'rails123', role: "admin"

# rails g scaffold Gate gate_number
# rails g scaffold Belt belt_number
# rails g scaffold Counter counter_number

# FlightStatus

# tod = ["San Francisco", "Chiang Mai", "Lisbon", "Sydney", "Budapest", "Bangkok", "Cape Town", "Chicago", "Dubai", "St. Petersburg", "Barcelona", "Buenos Aires", "Florence", "New York City", "Berlin", "Shanghai", "Siem Reap", "Hanoi", "Paris", "Marrakech"]
# # user = Flight.create! :email => 'test@test.com', :password => 'rails123', :password_confirmation => 'rails123'
# ind_tod ||= 0
# 20.times do 
# ind_tod += 1
# p tod.at(ind_tod)
# # num = rand(9999)
# num = Time.now.strftime("%M%S")
# n1 = "a,b,c,d,e,f,h,g,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,y,z"
# fl = n1.split(",").sample
# n2 = "a,b,c,d,e,f,h,g,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,y,z"
# ll = n2.split(",").sample
# fnum = fl.upcase+ll.upcase+num.to_s
# s_time = Time.now
# s_date = Date.today
#  Flight.create! status_type: "departure", flight_number: fnum, airline_name: "1", airport_name: "John F. Kennedy International Airport", via: "", airline_id: nil, from_to: tod.at(ind_tod), schedule_date: s_date, schedule_time: s_time, actual_date: s_date, actual_time: s_time, expected_time: s_time, gate_number: "4", counter_number: nil, belt_number: "1", flight_status: "Boarding",bgcolor: "blue"
# end

fromd = ["Prague", "Beijing", "London", "Rome", "Istanbul", "New Delhi", "Hyderabad", "Kilimanjaro", "Tabora", "Milford Sound", "Ngorongoro", "Victoria Falls", "Botswana", "Mount Mulanje", "Jakarta", "Mumbai", "Tehran", "Chennai", "Arusha", "Dar es Salaam"]
ind_fromd ||= 0
20.times do 
ind_fromd += 1
p ind_fromd
#num = rand(9999)
num = Time.now.strftime("%M%S")
n1 = "a,b,c,d,e,f,h,g,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,y,z"
fl = n1.split(",").sample
n2 = "a,b,c,d,e,f,h,g,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,y,z"
ll = n2.split(",").sample
fnum = fl.upcase+ll.upcase+num.to_s
s_time = Time.now
s_date = Date.today
 Flight.create! status_type: "arrival", flight_number: fnum, airline_name: "1", airport_name: "John F. Kennedy International Airport", via: "", airline_id: nil, from_to: fromd.at(ind_fromd), schedule_date: s_date, schedule_time: s_time, actual_date: s_date, actual_time: s_time, expected_time: s_time, gate_number: "4", counter_number: nil, belt_number: "1", flight_status: "Boarding",bgcolor: "blue"
end
# Flight.update_all(status_type: "arrival",["status_type = ? ","arrivals"])
# num = rand(9999)
# n1 = "a,b,c,d,e,f,h,g,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,y,z"
# fl = n1.split(",").sample
# n2 = "a,b,c,d,e,f,h,g,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,y,z"
# ll = n2.split(",").sample
# fnum = fl.upcase+ll.upcase+num.to_s

# Flight number:

# Airline name: South African Airways

# Airport name: Mwanza Airport

# From to:

# Schedule date: 2014-10-15

# Schedule time: 2000-01-01 14:45:00 UTC

# Actual date: 2014-10-15

# Actual time: 2000-01-01 14:45:00 UTC

# Expected time: 2000-01-01 14:45:00 UTC

 #  attr_accessible :actual_date, :actual_time, :airline_name, :airport_name, :belt_number, :counter_number, :expected_time, :flight_number, :flight_status, :from_to, :gate_number, :schedule_date, :schedule_time, :status_type, :via, :airline_id
 #  has_many :display
 #  belongs_to :airline
 #  validates :name, :presence => {:message => 'Name cannot be blank, Task not saved'}
 #  validates :col_a, :col_b, :col_c, :presence => true, :numericality => true
	# validates :airline_name, :airport_name, :actual_time,:actual_date, :actual_time,:actual_date,:from_to :presence => true#, :numericality => true

	# 

# ["San Francisco,Chiang Mai,Lisbon,Sydney,Budapest,Bangkok,Cape Town,Chicago,Dubai,St. Petersburg,Barcelona,Buenos Aires,Florence,New York City,Berlin,Shanghai,Siem Reap,Hanoi,Paris,Marrakech,Prague,Beijing,London,Rome,Istanbul,New Delhi,Hyderabad,Kilimanjaro,Tabora,Milford Sound,Ngorongoro,Victoria Falls,Botswana,Mount Mulanje,Jakarta,Mumbai,Tehran,Chennai,Arusha,Dar es Salaam"]

# tod = ["San Francisco", "Chiang Mai", "Lisbon", "Sydney", "Budapest", "Bangkok", "Cape Town", "Chicago", "Dubai", "St. Petersburg", "Barcelona", "Buenos Aires", "Florence", "New York City", "Berlin", "Shanghai", "Siem Reap", "Hanoi", "Paris", "Marrakech"]
# fromd = ["Prague", "Beijing", "London", "Rome", "Istanbul", "New Delhi", "Hyderabad", "Kilimanjaro", "Tabora", "Milford Sound", "Ngorongoro", "Victoria Falls", "Botswana", "Mount Mulanje", "Jakarta", "Mumbai", "Tehran", "Chennai", "Arusha", "Dar es Salaam"]
# It is a different framework,we can use Ember in rails for the client side MVC tasks
