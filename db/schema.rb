# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141208134621) do

  create_table "airlines", :force => true do |t|
    t.string   "airline_name"
    t.string   "iata"
    t.string   "logo"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "airport_details", :force => true do |t|
    t.string   "airport_name"
    t.string   "default_message"
    t.string   "footer_message"
    t.string   "airport_logo"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "airports", :force => true do |t|
    t.string   "airport_name"
    t.string   "local_name"
    t.string   "iata"
    t.string   "icao"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "belts", :force => true do |t|
    t.string   "belt_number"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "counter_displays", :force => true do |t|
    t.integer  "counter_number"
    t.string   "airline_image"
    t.string   "background_color"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "counters", :force => true do |t|
    t.string   "counter_number"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "displays", :force => true do |t|
    t.string   "display_name"
    t.integer  "flight_id"
    t.string   "description"
    t.string   "gate_number"
    t.string   "belt_number"
    t.string   "counter_number"
    t.string   "viewing"
    t.string   "flight_numbers"
    t.string   "schedule_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "flights", :force => true do |t|
    t.string   "status_type"
    t.string   "flight_number"
    t.string   "airline_name"
    t.string   "airport_name"
    t.string   "via"
    t.integer  "airline_id"
    t.string   "from_to"
    t.date     "schedule_date"
    t.time     "schedule_time"
    t.date     "actual_date"
    t.time     "actual_time"
    t.time     "expected_time"
    t.string   "gate_number"
    t.string   "counter_number"
    t.string   "belt_number"
    t.string   "flight_status"
    t.string   "bgcolor"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "gates", :force => true do |t|
    t.string   "gate_number"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "remarks", :force => true do |t|
    t.string   "flight_status"
    t.string   "local_name"
    t.string   "color"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "schedules", :force => true do |t|
    t.string   "status_type"
    t.string   "flight_number"
    t.string   "airline_name"
    t.string   "airport_name"
    t.string   "from_to"
    t.string   "via"
    t.time     "schedule_time"
    t.date     "valid_from"
    t.date     "valid_till"
    t.string   "available_days"
    t.string   "gate_number"
    t.string   "counter_number"
    t.string   "belt_number"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "role"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
