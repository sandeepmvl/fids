Fids::Application.routes.draw do

  resources :belt_displays


  resources :gate_displays


  resources :counter_displays do
    get :display_results , on: :member
  end


=begin
  get "display_boards/arrivals"

  get "display_boards/departures"

  get "display_boards/arrivals_depratures"

  get "display_boards/flights"

  get "display_boards/actions"
=end

  resources :display_boards do
    get :arrivals , on: :collection
    get :departures , on: :collection
    get :arrivals_depratures , on: :collection
    get :flights , on: :collection
    get :counters , on: :collection
    get :gates , on: :collection
    get :belts , on: :collection
    get :actions , on: :collection
    get :arrival_results, on: :collection
    get :departure_results, on: :collection
    get :flight_results, on: :collection
    get :counter_results, on: :collection
    get :gate_results, on: :collection
    get :belt_results, on: :collection
    get :arrival_departure_results, on: :collection
  end

  get "displays/flights_list"
  get "user_settings/add_user"
  get "user_settings/index"
  post "user_settings/create"
  post "user_settings/delete_user"
  post "flights/delete_multiple_flights"
  # resources :user_settings

  resources :displays

  resources :flights

  resources :remarks

  resources :schedules

  resources :counters

  resources :belts

  resources :gates

  resources :airports

  resources :airlines

  resources :airport_details

  match 'update_status' => 'flights#update_status', via: [:put,:get]

  devise_for :users
  devise_for :users do get '/users/sign_out' => 'devise/sessions#destroy' end
  
  devise_for :users, :skip => [:registrations]                                          
    as :user do
     get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'    
     put 'users/:id' => 'devise/registrations#update', :as => 'user_registration'            
  end


  # get "home/index"
  root :to => 'home#index'
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
