class Counter < ActiveRecord::Base
  attr_accessible :counter_number, :flight_id
 	validates :counter_number,presence: true, uniqueness: true, numericality: true
  # belongs_to :flight
end
