class Gate < ActiveRecord::Base
  attr_accessible :gate_number, :flight_id
  # belongs_to :flight
  validates :gate_number,presence: true, uniqueness: true, numericality: true
end
