class Schedule < ActiveRecord::Base
  attr_accessible :airline_name, :airport_name, :available_days, :belt_number, :counter_number, :flight_number, :gate_number, :from_to, :schedule_time, :status_type, :valid_from, :valid_till, :via
	validates :airline_name, :airport_name,:flight_number,:from_to,:schedule_time,:valid_till,:valid_from,:available_days,:presence => true
	validates :flight_number, uniqueness: {message: "is already created with same date and status type", if: :check_for_type_and_date_is_present}

  def check_for_type_and_date_is_present
    Flight.where(status_type: self.status_type,flight_number: self.flight_number,schedule_date: Date.today).present?
  end
end


# @departures = Flight.where('DATE(schedule_date) = ? and  schedule_time and status_type = (?)',Date.today,"departure").page(params[:page]).per(10)
# 45.minutes.ago > schedule_time
# 45.minutes.ago < expected_time

# Time.zone = TZInfo::Timezone.get('Asia/Kolkata')
# schedule_time > 45.minutes.ago.in_time_zone
# @departures = Flight.where('DATE(schedule_date) = ? and  schedule_time.strftime("%H:%M") > ? and status_type = (?)',Date.today,45.minutes.ago.in_time_zone.strftime("%H:%M"),"departure").page(params[:page]).per(10)

# Time.parse("10:10")