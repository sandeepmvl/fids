class Belt < ActiveRecord::Base
  attr_accessible :belt_number, :flight_id
  # belongs_to :flight
	validates :belt_number,presence: true, uniqueness: true, numericality: true
end
