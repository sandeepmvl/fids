class Airline < ActiveRecord::Base
  attr_accessible :airline_name, :iata, :logo#, :flight_id
  mount_uploader :logo, AirlinerUploader
  has_many :flights
  validates :airline_name, :iata,:logo,presence: true
  validates :airline_name, :iata,uniqueness: true
end


