class Remark < ActiveRecord::Base
  attr_accessible :color, :flight_status, :local_name
 	validates :flight_status,presence: true, uniqueness: true
 	validates_format_of :flight_status,with: /[a-z]/,message: "should contain only alphabets."
end
