class Airport < ActiveRecord::Base
  attr_accessible :airport_name, :iata, :icao, :local_name, :flight_id
  # belongs_to :flight
	validates :airport_name, :iata,:icao,presence: true,uniqueness: true
	validates :iata,:icao, uniqueness: { case_sensitive: false }
	validates_format_of :icao,:iata,:with => /[a-zA-Z]/,message: "ICAO/IATA codes should contain only alphabets."
end