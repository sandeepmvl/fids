class GateDisplay < ActiveRecord::Base
  attr_accessible :airline_image, :background_color, :counter_number
  mount_uploader :airline_image, AirlineLogoUploader
end
