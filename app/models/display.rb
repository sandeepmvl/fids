class Display < ActiveRecord::Base
  attr_accessible :description, :display_name, :schedule_type, :viewing,:gate_number,:belt_number,:counter_number, :flight_id
  belongs_to :flight
  validates :schedule_type,:display_name,:description, presence: true
end

