class Flight < ActiveRecord::Base
  attr_accessible :actual_date, :actual_time, :airline_name, :airport_name, :belt_number, :counter_number, :expected_time, :flight_number, :flight_status, :from_to, :gate_number, :schedule_date, :schedule_time, :status_type, :via, :airline_id, :bgcolor
  has_many :display
  belongs_to :airline
  validates  :flight_number,:from_to,presence: true
  validates_presence_of :airline_name, :message => "can't be empty, Please go and create Airline Name first"
  validates_presence_of :airport_name, :message => "can't be empty, Please go and create Airport Name first"
  validates_presence_of :flight_status, :message => "can't be empty, Please go and create Status first"
  # using a method:
  # validates :flight_number, uniqueness: true,message: "Please visit beeg.com", if: :check_for_type_and_date_is_present
  validates :flight_number, uniqueness: {message: "is already created with same date and status type", if: :check_for_type_and_date_is_present}

  def check_for_type_and_date_is_present
    Flight.where(status_type: self.status_type,flight_number: self.flight_number,schedule_date: Date.today).present?
  end
end
