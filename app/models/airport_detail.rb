class AirportDetail < ActiveRecord::Base
  attr_accessible :airport_logo, :airport_name, :default_message, :footer_message
  mount_uploader :airport_logo, AirlinerUploader
  validates :airport_logo, :airport_name, :default_message,:footer_message,presence: true
end