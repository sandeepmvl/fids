class DisplayBoardsController < ApplicationController
  layout "displayboards"
	

  def actions
    case params[:display]
    when "Arr"
      redirect_to arrivals_display_boards_path 
    when "Dept"
      # redirect_to display_boards_departures_path 
      redirect_to departures_display_boards_path 
    when "Arr/Dept"
      redirect_to arrivals_depratures_display_boards_path
    when "Gate"
      redirect_to gates_display_boards_path(gate_number: params[:gate_number],flight_numbers: params[:flight_numbers])
    when "Belt"
      redirect_to belts_display_boards_path(belt_number: params[:belt_number],flight_numbers: params[:flight_numbers])
    when "Counter"
      redirect_to counters_display_boards_path(counter_number: params[:counter_number],flight_numbers: params[:flight_numbers])
    when "Flights"
      redirect_to flights_display_boards_path(flight_number: params[:flight_number])
    end
	end

  def arrivals
    # @airport_logo = AirportDetail.first.airport_logo
    @page_number =  params[:page]
    @arrivals = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"arrival").page(params[:page]).per(10)
    respond_to do |format|
      format.html
      format.json { render json: @arrivals}
    end
  end

  def arrival_results
    @arrivals = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"arrival").page(params[:page]).per(10)
    respond_to do |format|
      format.js
    end
  end

  def departures
    @page_number =  params[:page]
		@departures = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"departure").page(params[:page]).per(10)
  end

  def departure_results
    @departures = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"departure").page(params[:page]).per(10)
    respond_to do |format|
      format.js
    end
  end

  def arrivals_depratures
    @page_number =  params[:page]
		@arrivals = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"arrival").page(params[:page]).per(4)
		@departures = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"departure").page(params[:page]).per(4)
  end

  def arrival_departure_results
    @arrivals = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"arrival").page(params[:page]).per(4)
    @departures = Flight.where('DATE(schedule_date) = ? and status_type = (?)',Date.today,"departure").page(params[:page]).per(4)
    respond_to do |format|
      format.js
    end
  end

  def flights
    @flight_number = params[:flight_number]
    @flights = Flight.where("DATE(schedule_date) = ? and id  = ?",Date.today, params[:flight_number])
    if @flights.first.nil? 
      @disp_val = "From/To" 
     elsif @flights.first.status_type == "arrival" 
      @disp_val = "From"
      @head_val = "Belt"
    else
     @disp_val = "To"
     @head_val = "Gate"
    end
  end

  def flight_results
    @flights = Flight.where("DATE(schedule_date) = ? and id  = ?",Date.today, params[:flight_number])
    @time = Date.today
    if @flights.first.nil? 
      @disp_val = "From/To" 
     elsif @flights.first.status_type == "arrival" 
      @disp_val = "From"
    else
     @disp_val = "To"
    end
    respond_to do |format|
      format.js
    end
  end

  def counters
    @flight_numbers = params[:flight_numbers]
    flights_list =  params[:flight_numbers].split(",") if @flight_numbers.present?
    @counters = Flight.where('counter_number = (?) and flight_number in (?)',params[:counter_number],(flights_list)).limit(2)
    # @counters = Flight.where('DATE(schedule_date) = ? and counter_number = (?) and flight_number in (?)',Date.today,params[:counter_number],(flights_list)).limit(2)
  end

  def counter_results
    flights_list =  params[:flight_numbers].split(",")
    @counters = Flight.where('counter_number = (?) and flight_number in (?)',params[:counter_number],(flights_list)).limit(2)
   # @counters = Flight.where('DATE(schedule_date) = ? and counter_number = (?) and flight_number in (?)',Date.today,params[:counter_number],(flights_list)).limit(2)
    respond_to do |format|
      format.json { render json: @counters.to_json(:include => { :airline => { :only => :logo }}) }
    end
  end

  def gates
    @flight_numbers = params[:flight_numbers]
    flights_list = params[:flight_numbers].split(",") rescue ""
    @gates = Flight.where('gate_number = (?) and flight_number in (?)',params[:gate_number],(flights_list)).limit(2)
  # @gates = Flight.where('DATE(schedule_date) = ? and gate_number = (?) and flight_number in (?)',Date.today,params[:gate_number],flights_list).limit(2)
  end

  def gate_results
    flights_list =  params[:flight_numbers].split(",")
    @gates = Flight.where('gate_number = (?) and flight_number in (?)',params[:gate_number],(flights_list)).limit(2)
    # @gates = Flight.where('DATE(schedule_date) = ? and gate_number = (?) and flight_number in (?)',Date.today,params[:gate_number],flights_list).limit(2)
    respond_to do |format|
     format.json { render json: @gates.to_json(:include => { :airline => { :only => :logo }}) }
    end
  end

  def belts
    @flight_numbers = params[:flight_numbers]
    flights_list =  params[:flight_numbers].split(",") if @flight_numbers.present?
    p flights_list
    @belts = Flight.where('belt_number = (?) and flight_number in (?)',params[:belt_number],(flights_list)).limit(2)
    # @belts = Flight.where('DATE(schedule_date) = ? and gate_number = (?) and flight_number in (?)',Date.today,params[:belt_number],flights_list).limit(2)
  end

  def belt_results
    flights_list =  params[:flight_numbers].split(",")
    @belts = Flight.where('belt_number = (?) and flight_number in (?)',params[:belt_number],(flights_list)).limit(2)
    # @belts = Flight.where('DATE(schedule_date) = ? and belt_number = (?) and flight_number in (?)',Date.today,params[:belt_number],flights_list).limit(2)
    respond_to do |format|
      format.json { render json: @belts.to_json(:include => { :airline => { :only => :logo }}) }
    end
  end
end