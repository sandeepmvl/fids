class GateDisplaysController < ApplicationController
  respond_to :html, :xml, :json
  layout "displayboards", only: [:show]
  before_filter :set_gate_display, only: [:show, :edit, :update, :destroy]

  def index
    @gate_displays = GateDisplay.all
    respond_with(@gate_displays)
  end

  def show
    respond_with(@gate_display)
  end

  def new
    @gate_display = GateDisplay.new
    respond_with(@gate_display)
  end

  def edit
  end

  def create
    @gate_display = GateDisplay.new(params[:gate_display])
    @gate_display.save
    respond_with(@gate_display)
  end

  def update
    @gate_display.update_attributes(params[:gate_display])
    respond_with(@gate_display)
  end

  def destroy
    @gate_display.destroy
    respond_with(@gate_display)
  end

  private
    def set_gate_display
      @gate_display = GateDisplay.find(params[:id])
    end
end
