class AirportDetailsController < ApplicationController
  before_filter :set_airport_detail, only: [:show, :edit, :update, :destroy]
  respond_to :html, :xml, :js

  def index
    @airport_details = AirportDetail.all
    respond_with(@airport_details)
  end

  def show
    respond_with(@airport_detail)
  end

  def new
    if AirportDetail.count < 1
      @airport_detail = AirportDetail.new
      respond_with(@airport_detail)
    else
      redirect_to airport_details_url(@airport_detail), status: :found
    end
  end

  def edit
  end

  def create
    @airport_detail = AirportDetail.new(params[:airport_detail])
    @airport_detail.save
    respond_with(@airport_detail)
  end

  def update
    @airport_detail.update_attributes(params[:airport_detail])
    respond_with(@airport_detail)
  end

  def destroy
    @airport_detail.destroy
    respond_with(@airport_detail)
  end

  private
    def set_airport_detail
      @airport_detail = AirportDetail.find(params[:id])
    end
end
