class UserSettingsController < ApplicationController
  # load_and_authorize_resource
  def index
  	@users = User.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def add_user
    @user_settings = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_setting }
    end
  end

  def show  
    @user = User.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @airline }
    end
  end

  def create
  	@user_settings = User.new(params[:user_settings])
    authorize! :create, @user_settings
    respond_to do |format|
      if @user_settings.save
        format.html { redirect_to user_settings_index_path, notice: 'User successfully created.' }
        format.json { render json: @user_settings, status: :created, location: @user_settings }
      else
        format.html { render action: "add_user" }
        format.json { render json: @user_settings.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_user
    25.times do 
      p "deleting user with id"
    end
    user = User.find(params[:user])
    25.times do 
      p "deleting user with id"
    end
    respond_to do |format|
      # if (current_user == user) and (user.role == "admin")
      #   format.html { redirect_to user_settings_index_path, notice: 'You cannot delete your own account or admin.' }
      if (current_user == user)
        format.html { redirect_to user_settings_index_path, alert: 'You cannot delete your own account.' }
      elsif (user.role == "admin") 
        format.html { redirect_to user_settings_index_path, alert: 'You cannot delete admin account' }
      elsif (user.role == "user") 
        format.html { redirect_to user_settings_index_path, alert: 'You cannot delete other users account.' }
      else
        user.delete
        format.html { redirect_to user_settings_index_path, alert: 'User successfully deleted.' }
      end
    end
  end
end
