class CounterDisplaysController < ApplicationController
  respond_to :html, :xml, :json
  layout "displayboards", only: [:show]
  before_filter :set_counter_display, only: [:show,:edit, :update, :destroy]

  def index
    @counter_displays = CounterDisplay.all
    respond_with(@counter_displays)
  end

  def show
    respond_with(@counter_display)
  end

  def new
    @counter_display = CounterDisplay.new
    respond_with(@counter_display)
  end

  def edit
  end

  def create
    @counter_display = CounterDisplay.new(params[:counter_display])
    @counter_display.save
    respond_with(@counter_display)
  end

  def update
    @counter_display.update_attributes(params[:counter_display])
    respond_with(@counter_display)
  end

  def destroy
    @counter_display.destroy
    respond_with(@counter_display)
  end

  def display_results
    @counter_display = CounterDisplay.find(params[:id])
    respond_to do |format|
      format.js
    end
  end


  private
    def set_counter_display
      @counter_display = CounterDisplay.find(params[:id])
    end
end
