class BeltDisplaysController < ApplicationController
  respond_to :html, :xml, :json
  layout "displayboards", only: [:show]
  before_filter :set_belt_display, only: [:show, :edit, :update, :destroy]

  def index
    @belt_displays = BeltDisplay.all
    respond_with(@belt_displays)
  end

  def show
    respond_with(@belt_display)
  end

  def new
    @belt_display = BeltDisplay.new
    respond_with(@belt_display)
  end

  def edit
  end

  def create
    @belt_display = BeltDisplay.new(params[:belt_display])
    @belt_display.save
    respond_with(@belt_display)
  end

  def update
    @belt_display.update_attributes(params[:belt_display])
    respond_with(@belt_display)
  end

  def destroy
    @belt_display.destroy
    respond_with(@belt_display)
  end

  private
    def set_belt_display
      @belt_display = BeltDisplay.find(params[:id])
    end
end
