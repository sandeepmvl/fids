class BeltsController < ApplicationController
  # GET /belts
  # GET /belts.json
  load_and_authorize_resource
  def index
    @belts = Belt.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @belts }
    end
  end

  # GET /belts/1
  # GET /belts/1.json
  def show
    @belt = Belt.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @belt }
    end
  end

  # GET /belts/new
  # GET /belts/new.json
  def new
    @belt = Belt.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @belt }
    end
  end

  # GET /belts/1/edit
  def edit
    @belt = Belt.find(params[:id])
  end

  # POST /belts
  # POST /belts.json
  def create
    @belt = Belt.new(params[:belt])

    respond_to do |format|
      if @belt.save
        format.html { redirect_to @belt, notice: 'Belt was successfully created.' }
        format.json { render json: @belt, status: :created, location: @belt }
      else
        format.html { render action: "new" }
        format.json { render json: @belt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /belts/1
  # PUT /belts/1.json
  def update
    @belt = Belt.find(params[:id])

    respond_to do |format|
      if @belt.update_attributes(params[:belt])
        format.html { redirect_to @belt, notice: 'Belt was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @belt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /belts/1
  # DELETE /belts/1.json
  def destroy
    @belt = Belt.find(params[:id])
    @belt.destroy

    respond_to do |format|
      format.html { redirect_to belts_url }
      format.json { head :no_content }
    end
  end
end
