class DisplaysController < ApplicationController
  # GET /displays
  # GET /displays.json
  load_and_authorize_resource
  def index
    # @displays = Display.all
    @displays = Display.order("created_at desc")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @displays }
    end
  end

  # GET /displays/1
  # GET /displays/1.json
  def show
    
    @display = Display.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @display }
    end
  end

  # GET /displays/new
  # GET /displays/new.json
  def new
    @display = Display.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @display }
    end
  end

  # GET /displays/1/edit
  def edit
    @display = Display.find(params[:id])
  end

  # POST /displays
  # POST /displays.json
  def create
    @display = Display.new(params[:display])
    @display.schedule_type = params[:schedule_type]
    @display.flight_id = params[:display][:viewing]
    @display.flight_numbers = params[:flight_number].join(",") if params[:flight_number].present?
    # @display.counter_number = Flight.find(params[:display][:counter_number]).counter_number if params[:display][:counter_number].present?
    # @display.belt_number = Flight.find(params[:display][:belt_number]).belt_number if params[:display][:belt_number].present?
    # @display.gate_number = Flight.find(params[:display][:gate_number]).gate_number if params[:display][:gate_number].present?
    # @display.schedule_type = params[:schedule_type]
    respond_to do |format|
      if @display.save
        format.html { redirect_to displays_path, notice: 'Display was successfully created.' }
        # format.json { render json: @displays, status: :created, location: @displays }
      else
        format.html { render action: "new" }
        format.json { render json: @display.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /displays/1
  # PUT /displays/1.json
  def update
    @display = Display.new(params[:display])
    @display.schedule_type = params[:schedule_type]
    @display.flight_id = params[:display][:viewing]
    @display.flight_numbers = params[:flight_number].join(",") if params[:flight_number].present?
    respond_to do |format|
      if @display.update_attributes(params[:display])
        format.html { redirect_to @display, notice: 'Display was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @display.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /displays/1
  # DELETE /displays/1.json
  def destroy
    @display = Display.find(params[:id])
    @display.destroy

    respond_to do |format|
      format.html { redirect_to displays_url, notice: 'Display was successfully deleted.'  }
      format.json { head :no_content }
    end
  end
  def counter_flight_list
        
    @flights_list = Flight.where("counter = (?)", 1)
    respond_to do |format|
      format.js
      # format.json { render json: @flights_list }
    end
  end

  # Get the flights from the value
  def flights_list 
    section_name = params.first[0]
    @section_name = params.first[0]
    section_number = params.first[1]
    @flights_list = Flight.where("#{section_name} = (?)", section_number)
    p @flights_list.count
    @selection_name = section_name
    respond_to do |format|
      format.js
      # format.json { render json: @flights_list }
    end
  end
end
