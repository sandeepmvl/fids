class SchedulesController < ApplicationController
  # GET /schedules
  # GET /schedules.json
  load_and_authorize_resource
  def index
    @schedules = Schedule.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @schedules }
    end
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
    @schedule = Schedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @schedule }
    end
  end

  # GET /schedules/new
  # GET /schedules/new.json
  def new
    @schedule = Schedule.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @schedule }
    end
  end

  # GET /schedules/1/edit
  def edit
    @schedule = Schedule.find(params[:id])
  end




  # POST /schedules
  # POST /schedules.json
  def create
    @schedule = Schedule.new(params[:schedule])
    p params
    @schedule.available_days = params[:schedule][:available_days].join(',') if params[:schedule][:available_days].present?
    @schedule.schedule_time = Time.parse(params[:schedule][:schedule_time]) if params[:schedule][:schedule_time].present?
    respond_to do |format|
      if @schedule.save
        day = params[:schedule][:available_days]
        range =  @schedule.valid_from..@schedule.valid_till
        # if range === Date.today && (day === Date.today.strftime("%A") || day.nil?)
        if range === Date.today && (day.include?(Date.today.strftime("%A")) || day.nil?)
          values = params[:schedule].except("valid_from(1i)","valid_from(2i)","valid_from(3i)","valid_till(3i)","valid_till(2i)","valid_till(1i)",:available_days)
          values[:schedule_date] = Date.today
          @flight.schedule_time = Time.parse(params[:schedule][:schedule_time]) if params[:schedule][:schedule_time].present?
          @flight =  Flight.new(values)
          if @flight.save
            format.html { redirect_to @schedule, notice: 'Schedule was successfully created.' }
            format.json { render json: @schedule, status: :created, location: @schedule }
          else
            format.html { render action: "new" }
            format.json { render json: @flight.errors, status: :unprocessable_entity }
          end
        end
        format.html { redirect_to @schedule, notice: 'Schedule was successfully created.' }
        format.json { render json: @schedule, status: :created, location: @schedule }
      else
        format.html { render action: "new" }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /schedules/1
  # PUT /schedules/1.json
  def update
    @schedule = Schedule.find(params[:id])

    respond_to do |format|
      if @schedule.update_attributes(params[:schedule])
        day = params[:schedule][:available_days]
        range =  @schedule.valid_from..@schedule.valid_till
        if range === Date.today && (day === Date.today.strftime("%A") || day.nil?)
          values = params[:schedule].except("valid_from(1i)","valid_from(2i)","valid_from(3i)","valid_till(3i)","valid_till(2i)","valid_till(1i)",:available_days)
          values[:schedule_date] = Date.today
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p params[:schedule][:flight_number]
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          p "*******************-----------------------***********************"
          # Flight.create!(values)
          flight = Flight.find_or_initialize_by_flight_number(params[:schedule][:flight_number])
          flight.update_attributes(values)
        end
        format.html { redirect_to @schedule, notice: 'Schedule was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @schedule = Schedule.find(params[:id])
    @schedule.destroy

    respond_to do |format|
      format.html { redirect_to schedules_url }
      format.json { head :no_content }
    end
  end
end

# flight = Flight.find_or_initialize_by_flight_number(flight_number)

# flight.update_attributes(
#  street_address: street_address,
#  city_name: city_name,
#  federalid: federalid,
#  state_prov_id: state_prov_id,
#  zip_code: zip_code
# )

# flight.update_attributes(values)

# status_type: string,
# flight_number: string,
# airline_name: string,
# airport_name: string,
# via: string,
# airline_id: integer,
# from_to: string,
# schedule_date: date,
# schedule_time: time,
# actual_date: date,
# actual_time: time,
# expected_time: time,
# gate_number: string,
# counter_number: string,
# belt_number: string,
# flight_status: string


# id: 24, status_type: "arrival", flight_number: "11889",
# airline_name: "Lufthansa", airport_name: "John F. Kennedy International Airport",
# from_to: "", via: "", schedule_time: "2000-01-01 17:44:00", valid_from: "2014-10-02", 
# valid_till: "2014-10-09", available_days: "Sunday,Friday,Saturday", gate_number: "4",
# counter_number: nil, belt_number: "1", created_at: "2014-10-02 15:44:35",
# updated_at: "2014-10-09 13:42:11"


        # Schedule.all.each do |schedule|
        #   day = schedule.available_days
        #   range =  schedule.valid_from..schedule.valid_till
        #   if range === Date.today && (day === Date.today.strftime("%A") || day.nil?)
        #     p "**********/////////***********----------------+++++++++++"
        #   end
        # end

#         range =  schedule.valid_from..schedule.valid_till
#         if range === Date.today && (day === Date.today.strftime("%A") || day.nil?)

        # Schedule.all.each do |schedule|
        #   day = schedule.available_days.gsub("\n", '').gsub("-","").split(" ")
        #   range =  schedule.valid_from..schedule.valid_till
        #   if range === Date.today && (day === Date.today.strftime("%A") || day.nil?)
        #     p "**********/////////***********----------------+++++++++++"
        #     p day
        #   end
        # end

  # schedules = Hash.new
  # Schedule.all.each do |schedule|
  #   day = schedule.available_days.gsub("\n", '').gsub("-","").split(" ")
  #   p day
  #   schedules.
  #   range =  schedule.valid_from..schedule.valid_till
  #   if range === Date.today && (day === Date.today.strftime("%A") || day.nil?)
  #     p "**********/////////***********----------------+++++++++++"
  #     p day
  #   end
  # end

  # .gsub("\n", '').gsub("-","").split(" ")
  # Flight.create!(schedule)