class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!
  helper_method :airport_message
  helper_method :airport_name
  helper_method :airport_footer
  helper_method :airport_logo

   # Catch all CanCan errors and alert the user of the exception
  rescue_from CanCan::AccessDenied do | exception |
    redirect_to root_url, alert: exception.message
  end
  
  def airport_message
    begin
      @airport_message = AirportDetail.last.default_message 
    rescue 
      @airport_message = "Airport Message change it in the controls section"
    end  
  end
 
  def airport_name
    begin
      AirportDetail.last.airport_name  
    rescue 
      "My airport change it in the controls section"
    end
  end
  def airport_logo
    if AirportDetail.last.present?
       AirportDetail.last.airport_logo  
    else
      "/assets/rsz_1rsz_airplane-512.png"  
    end
  end
  def airport_footer
    begin
      AirportDetail.last.footer_message  
    rescue 
      "We wish you happy and safe journey."      
    end
  end
end