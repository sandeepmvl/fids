class FlightsController < ApplicationController
  # GET /flights
  # GET /flights.json
  def index
    @flights = Flight.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @flights }
    end
  end

  # GET /flights/1
  # GET /flights/1.json
  def show
    @flight = Flight.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @flight }
    end
  end

  # GET /flights/new
  # GET /flights/new.json
  def new
    @flight = Flight.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @flight }
    end
  end

  # GET /flights/1/edit
  def edit
    @flight = Flight.find(params[:id])
  end

  # POST /flights
  # POST /flights.json
  def create
    @flight = Flight.new(params[:flight])

    #@flight.schedule_time = Time.parse(params[:flight][:schedule_time])

    # 50.times do
    # p "564864231864231245456444444444444444444444444444444444444444444444444444444444444"
    # p params[:flight][:airline_name]
    # end
    respond_to do |format|

      if @flight.save
        @flight.airline_name = Airline.find(params[:flight][:airline_name].to_i).airline_name
        @flight.airline_id = params[:flight][:airline_name].to_i
        @flight.bgcolor = Remark.where("flight_status = ?",@flight.flight_status).first.color
        @flight.save
        format.html { redirect_to @flight, notice: 'Flight was successfully created.' }
        format.json { render json: @flight, status: :created, location: @flight }
      else
        format.html { render action: "new" }
        format.json { render json: @flight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /flights/1
  # PUT /flights/1.json
  def update
    @flight = Flight.find(params[:id])

    respond_to do |format|
      if @flight.update_attributes(params[:flight])
        @flight.airline_name = Airline.find(params[:flight][:airline_name].to_i).airline_name
        @flight.airline_id = params[:flight][:airline_name].to_i
        @flight.bgcolor = Remark.where("flight_status = ?",@flight.flight_status).first.color
        @flight.save
        format.html { redirect_to @flight, notice: 'Flight was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @flight.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_status
    bgcolor = Remark.find_by_flight_status(params[:flight_details]).color
    Flight.find(params[:flight_id]).update_attributes(flight_status: params[:flight_details],bgcolor: bgcolor)
    redirect_to (:back)
  end
  # DELETE /flights/1
  # DELETE /flights/1.json
  def destroy
    @flight = Flight.find(params[:id])
    @flight.destroy

    respond_to do |format|
      format.html { redirect_to flights_url }
      format.json { head :no_content }
    end
  end

  def delete_multiple_flights
    # if params[:flights].present
    @flights_to_delete = Flight.where("id IN (?)",params[:flights])
    respond_to do |format|
      if @flights_to_delete.delete_all
        format.html { redirect_to flights_url, notice: 'Selected flights successfully deleted.' }
      else
        format.html { redirect_to flights_url, notice: 'Some thing went wrong!' }
      end
    end
  end
end