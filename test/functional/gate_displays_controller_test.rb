require 'test_helper'

class GateDisplaysControllerTest < ActionController::TestCase
  setup do
    @gate_display = gate_displays(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gate_displays)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gate_display" do
    assert_difference('GateDisplay.count') do
      post :create, gate_display: { airline_image: @gate_display.airline_image, background_color: @gate_display.background_color, counter_number: @gate_display.counter_number }
    end

    assert_redirected_to gate_display_path(assigns(:gate_display))
  end

  test "should show gate_display" do
    get :show, id: @gate_display
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gate_display
    assert_response :success
  end

  test "should update gate_display" do
    put :update, id: @gate_display, gate_display: { airline_image: @gate_display.airline_image, background_color: @gate_display.background_color, counter_number: @gate_display.counter_number }
    assert_redirected_to gate_display_path(assigns(:gate_display))
  end

  test "should destroy gate_display" do
    assert_difference('GateDisplay.count', -1) do
      delete :destroy, id: @gate_display
    end

    assert_redirected_to gate_displays_path
  end
end
