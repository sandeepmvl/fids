require 'test_helper'

class FlightsControllerTest < ActionController::TestCase
  setup do
    @flight = flights(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:flights)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create flight" do
    assert_difference('Flight.count') do
      post :create, flight: { actual_date: @flight.actual_date, actual_time: @flight.actual_time, airline_name: @flight.airline_name, airport_name: @flight.airport_name, belt_number: @flight.belt_number, counter_number: @flight.counter_number, expected_time: @flight.expected_time, flight_number: @flight.flight_number, flight_status: @flight.flight_status, from_to: @flight.from_to, gate_number: @flight.gate_number, schedule_date: @flight.schedule_date, schedule_time: @flight.schedule_time, status_type: @flight.status_type, via: @flight.via }
    end

    assert_redirected_to flight_path(assigns(:flight))
  end

  test "should show flight" do
    get :show, id: @flight
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @flight
    assert_response :success
  end

  test "should update flight" do
    put :update, id: @flight, flight: { actual_date: @flight.actual_date, actual_time: @flight.actual_time, airline_name: @flight.airline_name, airport_name: @flight.airport_name, belt_number: @flight.belt_number, counter_number: @flight.counter_number, expected_time: @flight.expected_time, flight_number: @flight.flight_number, flight_status: @flight.flight_status, from_to: @flight.from_to, gate_number: @flight.gate_number, schedule_date: @flight.schedule_date, schedule_time: @flight.schedule_time, status_type: @flight.status_type, via: @flight.via }
    assert_redirected_to flight_path(assigns(:flight))
  end

  test "should destroy flight" do
    assert_difference('Flight.count', -1) do
      delete :destroy, id: @flight
    end

    assert_redirected_to flights_path
  end
end
