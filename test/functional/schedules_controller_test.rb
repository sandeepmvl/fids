require 'test_helper'

class SchedulesControllerTest < ActionController::TestCase
  setup do
    @schedule = schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create schedule" do
    assert_difference('Schedule.count') do
      post :create, schedule: { airline_name: @schedule.airline_name, airport_name: @schedule.airport_name, available_days: @schedule.available_days, belt_number: @schedule.belt_number, counter_number: @schedule.counter_number, destination: @schedule.destination, flight_number: @schedule.flight_number, gate_number: @schedule.gate_number, origin: @schedule.origin, schedule_time: @schedule.schedule_time, status_type: @schedule.status_type, valid_from: @schedule.valid_from, valid_till: @schedule.valid_till, via: @schedule.via }
    end

    assert_redirected_to schedule_path(assigns(:schedule))
  end

  test "should show schedule" do
    get :show, id: @schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @schedule
    assert_response :success
  end

  test "should update schedule" do
    put :update, id: @schedule, schedule: { airline_name: @schedule.airline_name, airport_name: @schedule.airport_name, available_days: @schedule.available_days, belt_number: @schedule.belt_number, counter_number: @schedule.counter_number, destination: @schedule.destination, flight_number: @schedule.flight_number, gate_number: @schedule.gate_number, origin: @schedule.origin, schedule_time: @schedule.schedule_time, status_type: @schedule.status_type, valid_from: @schedule.valid_from, valid_till: @schedule.valid_till, via: @schedule.via }
    assert_redirected_to schedule_path(assigns(:schedule))
  end

  test "should destroy schedule" do
    assert_difference('Schedule.count', -1) do
      delete :destroy, id: @schedule
    end

    assert_redirected_to schedules_path
  end
end
