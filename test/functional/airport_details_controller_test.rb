require 'test_helper'

class AirportDetailsControllerTest < ActionController::TestCase
  setup do
    @airport_detail = airport_details(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:airport_details)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create airport_detail" do
    assert_difference('AirportDetail.count') do
      post :create, airport_detail: { airport_logo: @airport_detail.airport_logo, airport_name: @airport_detail.airport_name, default_message: @airport_detail.default_message, footer_message: @airport_detail.footer_message }
    end

    assert_redirected_to airport_detail_path(assigns(:airport_detail))
  end

  test "should show airport_detail" do
    get :show, id: @airport_detail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @airport_detail
    assert_response :success
  end

  test "should update airport_detail" do
    put :update, id: @airport_detail, airport_detail: { airport_logo: @airport_detail.airport_logo, airport_name: @airport_detail.airport_name, default_message: @airport_detail.default_message, footer_message: @airport_detail.footer_message }
    assert_redirected_to airport_detail_path(assigns(:airport_detail))
  end

  test "should destroy airport_detail" do
    assert_difference('AirportDetail.count', -1) do
      delete :destroy, id: @airport_detail
    end

    assert_redirected_to airport_details_path
  end
end
