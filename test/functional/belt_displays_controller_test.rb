require 'test_helper'

class BeltDisplaysControllerTest < ActionController::TestCase
  setup do
    @belt_display = belt_displays(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:belt_displays)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create belt_display" do
    assert_difference('BeltDisplay.count') do
      post :create, belt_display: { airline_image: @belt_display.airline_image, background_color: @belt_display.background_color, counter_number: @belt_display.counter_number }
    end

    assert_redirected_to belt_display_path(assigns(:belt_display))
  end

  test "should show belt_display" do
    get :show, id: @belt_display
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @belt_display
    assert_response :success
  end

  test "should update belt_display" do
    put :update, id: @belt_display, belt_display: { airline_image: @belt_display.airline_image, background_color: @belt_display.background_color, counter_number: @belt_display.counter_number }
    assert_redirected_to belt_display_path(assigns(:belt_display))
  end

  test "should destroy belt_display" do
    assert_difference('BeltDisplay.count', -1) do
      delete :destroy, id: @belt_display
    end

    assert_redirected_to belt_displays_path
  end
end
