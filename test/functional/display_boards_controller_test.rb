require 'test_helper'

class DisplayBoardsControllerTest < ActionController::TestCase
  test "should get arrivals" do
    get :arrivals
    assert_response :success
  end

  test "should get departures" do
    get :departures
    assert_response :success
  end

  test "should get arrivals_depratures" do
    get :arrivals_depratures
    assert_response :success
  end

  test "should get flights" do
    get :flights
    assert_response :success
  end

end
