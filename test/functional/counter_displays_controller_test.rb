require 'test_helper'

class CounterDisplaysControllerTest < ActionController::TestCase
  setup do
    @counter_display = counter_displays(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:counter_displays)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create counter_display" do
    assert_difference('CounterDisplay.count') do
      post :create, counter_display: { airline_image: @counter_display.airline_image, background_color: @counter_display.background_color, counter_number: @counter_display.counter_number }
    end

    assert_redirected_to counter_display_path(assigns(:counter_display))
  end

  test "should show counter_display" do
    get :show, id: @counter_display
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @counter_display
    assert_response :success
  end

  test "should update counter_display" do
    put :update, id: @counter_display, counter_display: { airline_image: @counter_display.airline_image, background_color: @counter_display.background_color, counter_number: @counter_display.counter_number }
    assert_redirected_to counter_display_path(assigns(:counter_display))
  end

  test "should destroy counter_display" do
    assert_difference('CounterDisplay.count', -1) do
      delete :destroy, id: @counter_display
    end

    assert_redirected_to counter_displays_path
  end
end
